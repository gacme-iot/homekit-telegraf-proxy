package de.gacme.iot.service;

import de.gacme.iot.adapter.output.rest.TelegrafRestClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class TelegrafServiceTest {

    public static final String MEASUREMENT_NAME = "my-measurement";
    public static final String SOURCE = "my-hostname";
    public static final String DEVICE = "my-device";
    @Mock
    TelegrafRestClient telegrafRestClient;

    @InjectMocks
    TelegrafService cut;

    @Nested
    class Temperature {

        @BeforeEach
        public void prepare() {
            cut.measurementName = MEASUREMENT_NAME;
            cut.source = SOURCE;
        }

        @Test
        void addTemperatureMeasurement_params() {

            String actual = cut.addTemperatureMeasurement(DEVICE, "17,1 °C", Map.of(
                    "room", List.of("ruempelkammer"),
                    "vendor", List.of("eve")
            ));

            String expected = "my-measurement,host=my-hostname,device=my-device,type=temperature,room=ruempelkammer,vendor=eve value=17.1";

            assertAll(
                    () -> assertEquals(expected, actual),
                    () -> verify(telegrafRestClient).submitMeasurement(expected)
            );
        }

        @Test
        void addTemperatureMeasurement_positive() {

            String actual = cut.addTemperatureMeasurement(DEVICE, "17,1 °C", Map.of());

            String expected = "my-measurement,host=my-hostname,device=my-device,type=temperature value=17.1";

            assertAll(
                    () -> assertEquals(expected, actual),
                    () -> verify(telegrafRestClient).submitMeasurement(expected)
            );
        }

        @Test
        void addTemperatureMeasurement_negative() {

            String actual = cut.addTemperatureMeasurement(DEVICE, "-17,1 °C", Map.of());

            String expected = "my-measurement,host=my-hostname,device=my-device,type=temperature value=-17.1";

            assertAll(
                    () -> assertEquals(expected, actual),
                    () -> verify(telegrafRestClient).submitMeasurement(expected)
            );
        }

        @Test
        void addTemperatureMeasurement_integer() {

            String actual = cut.addTemperatureMeasurement(DEVICE, "17 °C", Map.of());

            String expected = "my-measurement,host=my-hostname,device=my-device,type=temperature value=17.0";

            assertAll(
                    () -> assertEquals(expected, actual),
                    () -> verify(telegrafRestClient).submitMeasurement(expected)
            );
        }
    }

    @Nested
    class Heater {

        @BeforeEach
        public void prepare() {
            cut.measurementName = MEASUREMENT_NAME;
            cut.source = SOURCE;
        }

        @Test
        void addHeatState_heizen_on() {

            String actual = cut.addHeatState(DEVICE, "Heizen ...", Map.of());

            String expected = "my-measurement,host=my-hostname,device=my-device,type=heat_state value=1";

            assertAll(
                    () -> assertEquals(expected, actual),
                    () -> verify(telegrafRestClient).submitMeasurement(expected)
            );
        }

        @Test
        void addHeatState_heating_on() {

            String actual = cut.addHeatState(DEVICE, "Heating", Map.of());

            String expected = "my-measurement,host=my-hostname,device=my-device,type=heat_state value=1";

            assertAll(
                    () -> assertEquals(expected, actual),
                    () -> verify(telegrafRestClient).submitMeasurement(expected)
            );
        }

        @Test
        void addHeatState_aus_off() {

            String actual = cut.addHeatState(DEVICE, "Aus", Map.of());

            String expected = "my-measurement,host=my-hostname,device=my-device,type=heat_state value=0";

            assertAll(
                    () -> assertEquals(expected, actual),
                    () -> verify(telegrafRestClient).submitMeasurement(expected)
            );
        }

        @Test
        void addHeatState_off_off() {

            String actual = cut.addHeatState(DEVICE, "Off", Map.of());

            String expected = "my-measurement,host=my-hostname,device=my-device,type=heat_state value=0";

            assertAll(
                    () -> assertEquals(expected, actual),
                    () -> verify(telegrafRestClient).submitMeasurement(expected)
            );
        }
    }

    @Nested
    class Contact {

        @BeforeEach
        public void prepare() {
            cut.measurementName = MEASUREMENT_NAME;
            cut.source = SOURCE;
        }

        @Test
        void addContactState_geoffnet_open() {

            String actual = cut.addContactState(DEVICE, "Geöffnet", Map.of());

            String expected = "my-measurement,host=my-hostname,device=my-device,type=contact_state value=1";

            assertAll(
                    () -> assertEquals(expected, actual),
                    () -> verify(telegrafRestClient).submitMeasurement(expected)
            );
        }

        @Test
        void addContactState_open_open() {

            String actual = cut.addContactState(DEVICE, "Open", Map.of());

            String expected = "my-measurement,host=my-hostname,device=my-device,type=contact_state value=1";

            assertAll(
                    () -> assertEquals(expected, actual),
                    () -> verify(telegrafRestClient).submitMeasurement(expected)
            );
        }

        @Test
        void addContactState_geschlossen_closed() {

            String actual = cut.addContactState(DEVICE, "Geschlossen", Map.of());

            String expected = "my-measurement,host=my-hostname,device=my-device,type=contact_state value=0";

            assertAll(
                    () -> assertEquals(expected, actual),
                    () -> verify(telegrafRestClient).submitMeasurement(expected)
            );
        }


        @Test
        void addContactState_closed_closed() {

            String actual = cut.addContactState(DEVICE, "Closed", Map.of());

            String expected = "my-measurement,host=my-hostname,device=my-device,type=contact_state value=0";

            assertAll(
                    () -> assertEquals(expected, actual),
                    () -> verify(telegrafRestClient).submitMeasurement(expected)
            );
        }
    }
}
