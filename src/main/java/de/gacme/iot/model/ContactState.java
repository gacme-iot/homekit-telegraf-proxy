package de.gacme.iot.model;

import java.util.List;
import java.util.stream.Stream;

public enum ContactState {
    OPENED(1, List.of("Geöffnet", "Open")),
    CLOSED(0, List.of("Geschlossen", "Closed"));

    private final int value;
    private final List<String> contactStateValues;

    ContactState(int value, List<String> contactStateValues) {
        this.value = value;
        this.contactStateValues = contactStateValues;
    }

    public static ContactState byContactState(String contactStateValue) {
        return Stream.of(ContactState.values())
                .filter(state ->
                        state.contactStateValues
                                .stream()
                                .anyMatch(contactStateValue::equalsIgnoreCase)
                )
                .findFirst()
                .orElseThrow();
    }

    public int getValue() {
        return value;
    }
}
