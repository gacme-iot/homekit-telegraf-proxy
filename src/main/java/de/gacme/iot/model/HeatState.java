package de.gacme.iot.model;

import java.util.List;
import java.util.stream.Stream;

public enum HeatState {
    ON(1, List.of("Heizen ...", "Heating")),
    OFF(0, List.of("Aus", "Off"));

    private final int value;
    private final List<String> heatStateValues;

    HeatState(int value, List<String> heatStateValues) {
        this.value = value;
        this.heatStateValues = heatStateValues;
    }

    public static HeatState byHeatState(String heatStateValue) {
        return Stream.of(HeatState.values())
                .filter(state ->
                        state.heatStateValues
                                .stream()
                                .anyMatch(heatStateValue::equalsIgnoreCase)
                )
                .findFirst()
                .orElseThrow();
    }

    public int getValue() {
        return value;
    }

}
