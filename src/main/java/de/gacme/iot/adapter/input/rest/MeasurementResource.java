package de.gacme.iot.adapter.input.rest;

import de.gacme.iot.service.TelegrafService;
import io.smallrye.common.constraint.NotNull;
import org.jboss.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.PathSegment;
import javax.ws.rs.core.Response;

@Path("/measurement")
public class MeasurementResource {

    private static final Logger LOG = Logger.getLogger(MeasurementResource.class);
    @Inject
    TelegrafService telegrafService;

    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/temperature/{device:[a-z][_a-z0-9]{2,}}")
    public Response addTemperatureMeasurement(
            @PathParam("device") PathSegment deviceSegment,
            @NotNull String value
    ) {
        String device = deviceSegment.getPath();
        MultivaluedMap<String, String> parameters = deviceSegment.getMatrixParameters();

        return Response
                .status(200)
                .entity(telegrafService.addTemperatureMeasurement(device, value, parameters))
                .build();
    }

    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/heat/{device:[a-z][_a-z0-9]{2,}}")
    public Response addHeatState(
            @PathParam("device") PathSegment deviceSegment,
            @NotNull String value
    ) {
        String device = deviceSegment.getPath();
        MultivaluedMap<String, String> parameters = deviceSegment.getMatrixParameters();

        return Response
                .status(200)
                .entity(telegrafService.addHeatState(device, value, parameters))
                .build();
    }

    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/contact/{device:[a-z][_a-z0-9]{2,}}")
    public Response addContactState(
            @PathParam("device") PathSegment deviceSegment,
            @NotNull String value
    ) {
        String device = deviceSegment.getPath();
        MultivaluedMap<String, String> parameters = deviceSegment.getMatrixParameters();

        return Response
                .status(200)
                .entity(telegrafService.addContactState(device, value, parameters))
                .build();
    }

}