package de.gacme.iot.adapter.output.rest;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.POST;
import javax.ws.rs.Path;

@Path("/homekit")
@RegisterRestClient(configKey="telegraf-client")
public interface TelegrafRestClient {

    @POST
    void submitMeasurement(String body);
}
