package de.gacme.iot.service;

import de.gacme.iot.adapter.output.rest.TelegrafRestClient;
import de.gacme.iot.model.ContactState;
import de.gacme.iot.model.HeatState;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@ApplicationScoped
public class TelegrafService {
    private static final Pattern TEMPERATURE_VALUE_PATTERN = Pattern.compile("^([-+]?\\d+(,\\d+)?) °C");
    private static final String ENTRY_PAIR_FORMAT = "{0}={1}";
    private static final String ENTRIES_CONCAT_FORMAT = "{0},{1}";

    private static final Logger LOG = Logger.getLogger(TelegrafService.class);

    @ConfigProperty(name = "flux.measurement")
    String measurementName;

    @ConfigProperty(name = "flux.source")
    String source;

    @Inject
    @RestClient
    TelegrafRestClient telegrafRestClient;


    public String addTemperatureMeasurement(String device, String value, Map<String, List<String>> parameters) {

        LOG.info("Received temperature measurement for device " + device + ". Value is " + value);

        Matcher matcher = TEMPERATURE_VALUE_PATTERN.matcher(value);
        double temperature = Optional.of(matcher)
                .filter(Matcher::matches)
                .map(matcher1 -> matcher1.group(1))
                .map(s -> s.replaceFirst(",", "."))
                .map(Double::parseDouble)
                .orElseThrow();

        String telegrafBody = influxStyle("temperature", device, Double.toString(temperature), parameters);

        LOG.info("Send " + telegrafBody);
        telegrafRestClient.submitMeasurement(telegrafBody);

        LOG.info("Measurement for device " + device + " sent. Temperature is " + temperature);

        return telegrafBody;
    }

    public String addHeatState(String device, String value, Map<String, List<String>> parameters) {
        LOG.info("Received heat state for device " + device + ". Value is " + value);

        HeatState state = HeatState.byHeatState(value.trim());

        String telegrafBody = influxStyle("heat_state", device, Integer.toString(state.getValue()), parameters);

        LOG.info("Send " + telegrafBody);
        telegrafRestClient.submitMeasurement(telegrafBody);

        LOG.info("Heat state for device " + device + " sent. State is " + state);
        return telegrafBody;
    }


    public String addContactState(String device, String value, Map<String, List<String>> parameters) {
        LOG.info("Received contact state for device " + device + ". Value is " + value);

        ContactState state = ContactState.byContactState(value.trim());

        String telegrafBody = influxStyle("contact_state", device, Integer.toString(state.getValue()), parameters);

        LOG.info("Send " + telegrafBody);
        telegrafRestClient.submitMeasurement(telegrafBody);

        LOG.info("Contact state for device " + device + " sent. State is " + state);
        return telegrafBody;
    }

    public String influxStyle(String type, String device, String value, Map<String, List<String>> parameters) {


        String influxMessageInit = measurementName + ",host=" + source + ",device=" + device + ",type=" + type;

        String influxMessage = parameters.entrySet()
                .stream()
                .map(entry ->
                        entry.getValue()
                                .stream()
                                .findFirst()
                                .map(paramValue -> MessageFormat.format(ENTRY_PAIR_FORMAT, entry.getKey(), paramValue))
                )
                .filter(Optional::isPresent)
                .map(Optional::get)
                .sorted()
                .reduce(influxMessageInit, (p1, p2) -> MessageFormat.format(ENTRIES_CONCAT_FORMAT, p1, p2));

        return influxMessage + " value=" + value;
    }

}
