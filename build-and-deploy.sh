# find . -exec dos2unix {} \;
#!/bin/bash
set -e

docker build -f src/main/docker/Dockerfile.native-micro --progress=plain -t amperboy/homekit-telegraf-proxy .
docker push amperboy/homekit-telegraf-proxy

# docker pull amperboy/homekit-telegraf-proxy && docker stop homekit-telegraf-proxy && docker rm homekit-telegraf-proxy
# docker run --name homekit-telegraf-proxy -d --restart always -p 8985:8080 amperboy/homekit-telegraf-proxy